# Spickzettel

## SPARQL-Anfragen

### Einfache Abfrage

```sparql
SELECT ?s WHERE { ?s rdfs:label "Universitätsbibliothek Basel"}
```

### Komplexere Abfrage mit Regex

```sparql
SELECT ?s WHERE { ?s rdfs:label ?o. FILTER regex(?o, "Universitätsbibliothek Basel", "i")}
```

### Implizit verwendete Namespaces

=> Thematisierung Namespaces anhand http://dbpedia.org/resource/Basel_University_Library => `dbr`

### Informationen zur UB

```
SELECT ?p ?o WHERE { dbr:Basel_University_Library ?p ?o} LIMIT 200
```

## OAI-PMH

### Abfrage mit Base-URL

=> Fehlermeldung thematisieren

### Allgemeine Informationen zum Repository

```
http://oai.swissbib.ch/oai/DB=2.1/?verb=Identify
```

### Unterstützte Metadatenformate auflisten

```
http://oai.swissbib.ch/oai/DB=2.1/?verb=ListMetadataFormats
```

### Vorhandene Sets auflisten

```
http://oai.swissbib.ch/oai/DB=2.1/?verb=ListSets
```

### Suche nach im September hinzugefügten Büchern

```
http://oai.swissbib.ch/oai/DB=2.1/?verb=ListIdentifiers&metadataPrefix=m21-xml%2Foai&from=2018-09-01&until=2018-09-30&set=A
```

### Hole einzelnen Record

```
http://oai.swissbib.ch/oai/DB=2.1/?verb=GetRecord&&metadataPrefix=m21-xml%2Foai&identifier=525860061
```
